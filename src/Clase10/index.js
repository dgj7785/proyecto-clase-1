const chalk = require('chalk');
const fs = require ('fs');
const coolImages = require("cool-images");
const express = require ('express');
const app = express();
const archivos = require("./archivo");

//const port = 3000
var port = process.env.PORT || '5000';
let bancImg = [];
let imgHis = [];

bancImg = coolImages.many(200, 400, 9);
imgHis.push(bancImg);

console.log(imgHis);
//imgHis.forEach(function (imgH,index) {
//   console.table(`imagen acumulada numero ${index + 1}: ${imgH}`);
//});

imgHis.forEach(function(value, index){
    archivos.write("./log.txt", value)
});


console.table(bancImg);

app.get('/', (req, res) => {
    res.send(`
    <h2 class="textoCentroColor">Galeria de imagenes con cool-images</h2>
    <div></div>
    <table>
    <thead>
        <tr>
            <td><img src="${bancImg[0]}" alt=""></td>
            <td><img src="${bancImg[1]}" alt=""></td> 
            <td><img src="${bancImg[2]}" alt=""></td> 
        </tr> 
    </thead>
    <tbody>
        <tr>
            <td><img src="${bancImg[3]}" alt=""></td>
            <td><img src="${bancImg[4]}" alt=""></td>
            <td><img src="${bancImg[5]}" alt=""></td> 
        </tr>
        <tr>
            <td><img src="${bancImg[6]}" alt=""></td>
            <td><img src="${bancImg[7]}" alt=""></td>
            <td><img src="${bancImg[8]}" alt=""></td> 
        </tr>
    </tbody>`)
})

app.listen(port, () => {
    console.log(chalk.red(`Aplicación de ejemplo escuchando en http://localhost:${chalk.underline.bgBlue(port)}`))
})
//console.log(chalk.blue('Challenge 10 - Acamica'));
//console.log(chalk.red.bgYellow('Challenge 10 - Acamica'));