const chalk = require('chalk');
const log = console.log;

log(chalk.blue('NPM') + ' Challenge' + chalk.red('!'));

log(chalk.blue.bgRed.bold('Acamica!'));

log(chalk.blue('Casa', 'treN', 'Auto', 'Vaca', 'CAfe', 'luz'));

log(chalk.red('NPM', chalk.underline.bgBlue('Acamica') + '!'));

log(chalk.green(
	'En verde ' +
	chalk.blue.underline.bold('azul subrayado') +
	' y verde de nuevo!'
));

// Con template literal
log(`
CPU: ${chalk.red('90%')}
RAM: ${chalk.green('40%')}
DISK: ${chalk.yellow('70%')}
`);
