const express = require('express');
const app = express();
const puerto = 3004;

const telefonos = [
{
    marca: 'Samsung',
    gama: 'alta',
    modelo: 'Galaxy S7',
    precio: '1299',
    pantalla: '6',
    sistema_operativo : 'Android'
},
{
    marca: 'Apple',
    gama: 'alta',
    modelo: 'iPhone 6',
    precio: '1299',
    pantalla: '6',
    sistema_operativo: 'iOS'
},
{
    marca: 'Xiaomi',
    gama: 'media',
    modelo: 'Redmi Note 3',
    precio: '9',
    pantalla: '4',
    sistema_operativo: 'Android'
},
{
    marca: 'Realme',
    gama: 'media',
    modelo: '7',
    precio: '244',
    pantalla: '5',
    sistema_operativo: 'Android'
},
{
    marca: 'Nokia',
    gama: 'baja',
    modelo: '1100',
    precio: '20',
    pantalla: '1.3',
    sistema_operativo: 'Symbian'
},
{
    marca: 'Sony Ericsson',
    gama: 'alta',
    modelo: 'k850',
    precio: '544',
    pantalla: '5',
    sistema_operativo: 'Java'
}];

app.get('/telefonos', (req, res) => {
    res.json(telefonos);
});

app.get('/gamaAlta', (req, res) => {
    res.json(telefonos.filter(telefono => telefono.gama === 'alta'));
});
// muestro minimo
app.get('/precioMin', (req, res) => {
    res.json(telefonos.filter(telefono => telefono.precio == min));
console.log(min)
});
// muestro maximo
app.get('/precioMax', (req, res) => {
    res.json(telefonos.filter(telefono => telefono.precio == max));
console.log(max)
});

app.get('/mitad', (req, res) => {
    res.json(telefonos.slice(0, telefonos.length / 2));

});

app.get ('/ordenado', (req, res) => {
    res.json(telefonos.sort(telefonos.modelo));
});
// busco minimo
let min = parseInt(telefonos[0].precio)
for (var i = 0; i < telefonos.length ; i++) {
    if (parseInt(telefonos[i].precio) < min) {
        min = telefonos[i].precio;
    }
}
// busco maximo
let max = parseInt(telefonos[0].precio)
for (var i = 0; i < telefonos.length ; i++) {
    if (parseInt(telefonos[i].precio) > max) {
        max = telefonos[i].precio;
    }
}
app.listen(puerto, () => {
    console.log(`Servidor corriendo en http://localhost:${puerto}`);
});
