const logger = (req, res, next) => {

    console.log(`request HTTP method: ${req.method}`);
    
    next();
    
    }
exports.logger = logger;