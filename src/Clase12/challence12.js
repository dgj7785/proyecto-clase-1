const express = require('express');
const app = express();
const port = 5000
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');


const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'Acamica API',
            version: '1.0.0'
        }
    },
    apis: ['./challence12.js'],
};  

const swaggerDocs = swaggerJsDoc(swaggerOptions);

app.use('/api-docs',
    swaggerUI.serve,
    swaggerUI.setup(swaggerDocs));

app.listen(port, () => console.log(`Servidor corriendo en http://localhost:${port}`));


/**
 * @swagger
 * /Cursos:
 *  get:
 *
 */
app.get('/cursos', (req, res) => {
    res.send([
        { id: 1, nombre: "NodeJs"},{id: 2, nombre: "java"},{id: 3, nombre: "Corte y Confeccion"}
    ])
}); 

/**
 * @swagger
 * /Cursos:
 *  post:
 *
 */
app.post('/cursos', (req, res) => {
    res.status(201).send();
});

const logger = (req, res, next) => {

    console.log(`request HTTP method: ${req.method}`);
    
    next();

}

app.use(logger);


