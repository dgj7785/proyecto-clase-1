const express = require('express');
const app = express();
const port = 5000
//const logger = require('logger');

app.listen(port, () => console.log(`Servidor corriendo en http://localhost:${port}`));



/* app.get('/estudiantes', (req, res) => {
    res.send([
        { id: 1, nombre: "Lucas", edad: 35 }
    ])
}); 
 */
app.post('/estudiantes', (req, res) => {
    res.status(201).send();
});

const logger = (req, res, next) => {

    console.log(`request HTTP method: ${req.method}`);
    
    next();
    
    }

const esAdministrador = (req, res, next) => {
    
    const usuarioAdministrador = true;
    
        if(usuarioAdministrador ) {
    
            console.log(`El usuario está correctamente
    
            logueado.`);
    
        next();
    
    } else {
    
        res.send(`No está logueado`);
    
        }
    };
/**
 * @swagger
 * /Estudiantes:
 *  get:
 *
 */
app.get('/estudiantes', esAdministrador, (req, res) => {
    res.send([
    {id: 1, nombre: "Lucas", edad: 35}
    ])
});

app.use(logger);

